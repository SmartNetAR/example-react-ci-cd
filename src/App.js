import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Aplicación de ejemplo para el uso de CI-CD de GitLab
        </p>
        <a
          className="App-link"
          href="https://gitlab.com/SmartNetAR/example-react-ci-cd"
          target="_blank"
          rel="noopener noreferrer"
        >
          Repo disponible
        </a>
        <a
          className="App-link"
          href="https://gitlab.com/SmartNetAR/example-react-ci-cd/-/blob/master/.gitlab-ci.yml"
          target="_blank"
          rel="noopener noreferrer"
        >
          .gitlab-ci.yml
        </a>
      </header>
    </div>
  );
}

export default App;
